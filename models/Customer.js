const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CustomerSchema = new Schema({
    first_name : {
        type : String,
        required : true
    },
    last_name : {
        type : String,
        required : true
    },
    customer_code : {
        type : String,
        required : true
    },
    address : {
        type : String,
        required : true
    },
    country : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    contact_no : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    }
})

module.exports = User = mongoose.model('customers', CustomerSchema)