const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TransactionSchema = new Schema({
    agent_code : {
        type : String,
        required : true
    },
    reciever_name : {
        type : String,
        required : true
    },
    to : {
        type : String,
        required : true
    },
    bank_name : {
        type : String,
        required : true
    },
    bsb_num : {
        type : String,
        required : true
    },
    reciever_account : {
        type : String,
        required : true
    },
    amount : {
        type : String,
        required : true
    },
    date : {
        type : Date,
        required : true
    },
    reference_no : {
        type : String,
        required : true
    },
    transfer_status : {
        type : String,
        required :true
    },
    
})

module.exports = User = mongoose.model('transactions', TransactionSchema)