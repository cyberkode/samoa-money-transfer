const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AgentSchema = new Schema({
    first_name : {
        type : String,
        required : true
    },
    last_name : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    agent_code : {
        type : String,
        required : true
    },
    address : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    serviceArea : {
        type : String,
        required : true
    },
    contact_no : {
        type : String,
        required : true
    },
})

module.exports = User = mongoose.model('agents', AgentSchema)