// server.js
const express = require('express');
const app = express();
const path = require('path');

const PORT = process.env.PORT || 5000;

// require db connection
require('./models/dbConn');

// configure body parser for AJAX requests
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static((__dirname, "./client/build")))

// routes
const customerRoutes = require('./controllers/CustomerController')
const transactionRoutes = require('./controllers/TransactionController');

app.use('/customer',customerRoutes);
app.use('/transaction', transactionRoutes);

app.get('*',(req, res) => {
    res.sendFile(path.join(__dirname, '/', './client/build', 'index.html' ));
});


// Bootstrap server
app.listen(PORT, () => {
	console.log(`Server listening on port ${PORT}.`);
});