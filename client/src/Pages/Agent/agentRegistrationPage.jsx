
import React,{Component} from 'react'
import AgentRegistrationForm from '../../Components/AgentRegistrationFormComponent/AgentRegistrationForm.component'

class AgentRegistrationPage extends Component {

    render() {
      return (
        <div className="container">
            <AgentRegistrationForm/>
        </div>
      );
    }
  }
  
  export default AgentRegistrationPage;