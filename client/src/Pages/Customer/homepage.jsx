import React from "react";

//import '../Navbar/navbar.css';

import LandingFirstSection from "../../Components/LandingComponent/landing.component";

const LandingPage = () => {
  return (
    <section className="container-fluid">
      {" "}
      <LandingFirstSection />
    </section>
  );
};

export default LandingPage;
