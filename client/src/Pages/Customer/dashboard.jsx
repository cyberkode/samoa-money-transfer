import React, { Component } from 'react';
import CustomerProfile from '../../Components/Profile/profile'
// import jwt_decode from 'jwt-decode'
import Axios from 'axios';



class Profile extends Component {
    constructor(){
        super()
        this.state = {
            token:'',
            user: '',
            tokenPresent: false
        }
    }

    componentDidMount() {
        const token = localStorage.usertoken
        Axios.get('http://localhost:5000/customer/profile/', {headers:{Authorization: 'Bearer '+token}})
            .then(data => {
                if(data.data.code === 200){
                    this.setState({
                        token:token,
                        user: data.data.user,
                        tokenPresent: true,
                    })
                }else{
                    this.setState({
                        tokenPresent: false
                    })
                }
            }).catch(error =>{
                console.log('error')
            })
        
        
    }

    render() {
        return (
            this.state.tokenPresent
            ? <CustomerProfile token={this.state.user} />
            : <div>Please sign in again</div> 
            // <div>Hello {this.state.user.first_name}</div> 
        )

    }

}

export default Profile;