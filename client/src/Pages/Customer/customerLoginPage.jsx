import React from 'react';
import { useForm } from 'react-hook-form';
import { CustomerService } from '../../Services'

const CustomerSignIn = (props) => {

    const onSubmit = (data) => {
        const { history } = props;
        CustomerService.customerLogin(data, history);
      };

    const { register, handleSubmit } = useForm();


    return (
        <div className="container-fluid">
            <h1 className="text-center" style={{paddingTop:50, paddingBottom:50}}>
                Customer Login
            </h1>
            <div class="row">
                <div class="col-lg-4 col-md-2">
                    
                </div>
                <div class="col-lg-4 col-md-8 col-sm-12 ">
                    <form noValidate onSubmit={handleSubmit(onSubmit)}>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" autoComplete="email" ref={register} />
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" autoComplete="password" ref={register} />
                        </div>
                        <button style={{marginTop:50, marginBottom:20}} type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </form>
                </div>
                <div class="col-lg-4 col-md-2">
                    
                </div>
            </div>
        </div>
    );
}

export default CustomerSignIn;
