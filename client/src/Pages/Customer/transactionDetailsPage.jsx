import React from "react";

import CustomerTransactionDetails from "../../Components/TransactionDetails/transactiondetails.component";

const TransactionDetailsPage = () => {
  return (
    <div className="container">
      <CustomerTransactionDetails />
    </div>
  );
};

export default TransactionDetailsPage;
