import React,{Component} from 'react'
import CustomerSignupForm from '../../Components/CustomerSignupFormComponent/customerSignup.component'

class CustomerSignupPage extends Component {

    render() {
      return (
        <div className="container-fluid">
            <CustomerSignupForm />
        </div>
      );
    }
  }
  
  export default CustomerSignupPage;