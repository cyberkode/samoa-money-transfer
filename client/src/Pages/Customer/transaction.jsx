import React from "react";

import Transaction from "../../Components/TransactionForm/transaction.component";

class CustomerTransactionPage extends React.Component {
  render() {
    return (
      <div>
        Transaction Page
        <Transaction />
      </div>
    );
  }
}

export default CustomerTransactionPage;
