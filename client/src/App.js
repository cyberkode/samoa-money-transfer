import React, { Component } from 'react';
import './App.css';
import {Route, BrowserRouter as Router,Switch} from 'react-router-dom';
import LandingPage from './Pages/Customer/homepage'
import CustomerLogin from './Pages/Customer/customerLoginPage'
import CustomerProfile from './Pages/Customer/dashboard'
import CustomerSignup from './Pages/Customer/customerSignupPage'
import AgentRegistrationPage from './Pages/Agent/agentRegistrationPage'
import CustomerTransactionPage from './Pages/Customer/transaction'
import NavBar from './Components/Navbar/navbar.compoent';
// import CustomerSignUp from ''
//pages
import CustomerTransactionDePage from './Pages/Customer/transactionDetailsPage';

class App extends Component{
  
  render() {
    const App = () => (
      <div>
        <Switch>
          {/* 
          <Route path='/agentRegistration' component={AgentRegistrationPage}/>
          <Route path='/agentDashboard' component={AgentDashboardPage}/>
          <Route path='/customerRegistration' component={CustomerRegistrationPage}/>
          <Route path='/customerLogin' component={CustomerLoginPage}/>
          <Route path='/profile' component={CustomerProfile}/> */}
          <Route exact path='/' component={LandingPage}/>
          <Route path='/agentRegistration' component={AgentRegistrationPage}/>
          <Route path='/customersignup' component={CustomerSignup}  />
          <Route exact path='/customerlogin' component={CustomerLogin}/>
          <Route path='/customerProfile' component={CustomerProfile}  />
          <Route path='/transaction' component={CustomerTransactionPage}/>
          <Route path='/transactiondetails' component={CustomerTransactionDePage} />
        </Switch>
      </div>
    )
    return (
      <Router>
        <NavBar/>
      <Switch>
        <App/>
      </Switch>
      {/* <Footer/> */}
      </Router>
    );
  }
}

export default App;
