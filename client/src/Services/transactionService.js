import Axios from "axios";
import Swal from "sweetalert2";

//Create Transaction
export function CreateTransaction(data, history) {
  const requestData = {
    agentName: data.agent_list,
    recipinpentName: data.recipientName,
    re_country: data.re_country,
    bankName: data.bankName,
    bsbNo: data.bsbNo,
    accNo: data.acc_no,
    trAmount: data.amount,
  };
  Axios.post("http://localhost:5000/transaction/reqtransaction", requestData)
    .then((res) => {
      if (res.status === 200) {
        console.log("Transaction created");
        Swal.fire({
          title: "Transaction Succesfull",
          text: "Do you want to do another Transaction?",
          icon: "success",
          showCancelButton: true,
          confirmButtonColor: "green",
          cancelButtonColor: "#d33",
          cancelButtonText: "No!",
          confirmButtonText: "Yes!",
        })
          .then((result) => {
            if (result.value) {
              history.push(`/agentRegistration`);
            } else {
              Swal.fire({
                title: "Transactin Receipt",
                html: `<ol>
              <li>${requestData.recipinpentName}</li>
              <li>${requestData.re_country}</li>
              <li>${requestData.agentName}</li>
              <li>${requestData.bankName}</li>
              <li>${requestData.bsbNo}</li>
              <li>${requestData.accNo}</li>
              <li>${requestData.trAmount}</li>
              </ol>`,
              });
            }
          })
          .catch((err) => {
            console.log("Swal error " + err);
          });
      } else if (res.status === 400) {
        console.log(res.statusText);
      } else {
        console.log("Something went wrong");
      }
    })
    .catch((err) => {
      console.log("post error " + err);
    });
}
