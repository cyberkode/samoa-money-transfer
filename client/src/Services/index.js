import * as AgentService from './agentServices'
import * as CustomerService from './customerServices'
import * as TransactionService from './transactionService'

export {
    AgentService,
    CustomerService,
    TransactionService
}