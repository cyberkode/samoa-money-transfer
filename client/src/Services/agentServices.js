import Axios from 'axios';

// Create new admin
export function createNewAdmin(data, history) {
    const reqData = {
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email,
        password: data.password,
        address:data.addressNo+","+data.addressStreet+","+data.addressCountry,
        service_area: data.service_area,
    };
  
    Axios.post( "http://localhost:4000/auth/register/", reqData)
      .then(() => {
        //Swal.fire('Good job!', 'Welcome To MalBay!', 'success');
        //history.push(`/signIn`);
      })
      .catch((err) => {
        console.error(err);
        // Swal.fire({
        //   icon: 'error',
        //   title: 'Oops...',
        //   text: 'Something went wrong!',
        // });
      });
  }