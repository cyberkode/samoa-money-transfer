import Axios from 'axios';
import Swal from 'sweetalert2'

export function customerLogin(data, history) {
    Axios.post('http://localhost:5000/customer/signin/', data)
        .then((res) => {
            if (res) {
                if (res.data.code === 200) {
                    localStorage.setItem('usertoken', res.data.token);
                    history.push(`/customerProfile`);
                } else if (res.data.code === 401) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: res.data.error,
                        footer: '<a href>Forgot password?</a>'
                    })
                } else if (res.data.code === 400) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: res.data.error,
                        footer: '<a href>Forgot password?</a>'
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    })
                }
            }
        })
        .catch((err) => console.log(err));
}

export function customerSignUp(data, history) {
    Axios.post('http://localhost:5000/customer/signup/', data).then(res => {
        if (res.data.code === 200) {
            Swal({
                title: "Success",
                text: "You've successfully signed up!",
                icon: "success"
            });
            history.push(`/customerProfile`);
        } else if (res.data.code === 400) {
            Swal({
                title: "Error",
                text: "Something went wrong",
                icon: "error"
            });
        } else {
            Swal({
                title: "Error",
                text: "Something went wrong",
                icon: "error"
            });
        }
    }).catch(error => {
        Swal({
            title: "Error",
            text: "Something went wrong",
            icon: "error"
        });
    })
}