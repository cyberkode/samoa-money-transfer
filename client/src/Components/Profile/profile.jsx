import React, { useState, useEffect } from 'react';

const CustomerProfile = (props) => {
    const [token, setToken] = useState({});
    // eslint-disable-next-line
    useEffect(() => {
        setToken(props.token)
    });


    return (
        <div className="container">
            <h2>Welcome {token.first_name} !</h2>
        </div>
    )
}

export default CustomerProfile