import React from "react";
import img1 from "../../assets/img/sPart1.svg";

import "../Navbar/navbar.css";

const LandingFirstSection = () => {
  return (
    <section className="container-fluid">
      <div className="row">
        <div className="col">
          <div className="calculate-form">
            {" "}
            <h3 className="text-center">
              Simple,Secure International Money Transfer
            </h3>
            <form className="contact-form p-5">
              
                <div className="form-group">
                  <label for="country">Send to</label>
                  <select
                    class="custom-select d-block w-100"
                    name="addressCountry"
                    id="country"
                    required
                  >
                    <option value="">Please Select Sending Country</option>
                    <option>Samoa Island</option>
                    <option>Newzeland</option>
                    <option>Australia</option>
                  </select>
                  <div class="invalid-feedback">
                    Please select a valid country.
                  </div>
              </div>
            </form>
          </div>
        </div>
        <div className="col">
          <div className="image-sec">
            <img src={img1} alt="heroImg" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default LandingFirstSection;
