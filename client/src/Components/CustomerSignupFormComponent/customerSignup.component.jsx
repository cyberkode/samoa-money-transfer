import React from "react";
import { useForm } from "react-hook-form";
import { CustomerService } from '../../Services'

const CustomerSignUp = (props) => {
    const { history } = props
    const onSubmit = async (data) => {
        CustomerService.customerSignUp(data, history)
    }
    const { signup, handleSubmit } = useForm();

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-7">
                    <div className="container" style={{padding:100}}>
                        <p class="h2 text-center">Customer Registration</p><br />
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div class="form-row">
                                <div class="col">
                                    <label for="first_name">First Name</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="first_name"
                                        name="first_name"
                                        autoComplete="given-name"
                                        ref={signup} />
                                </div>
                                <div class="col">
                                    <label for="last_name">Last Name</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="last_name"
                                        name="last_name"
                                        autoComplete="family-name"
                                        ref={signup} />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input
                                    type="email"
                                    class="form-control"
                                    id="email"
                                    name="email"
                                    aria-describedby="emailHelp"
                                    autoComplete="email"
                                    ref={signup} />
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input
                                    type="password"
                                    class="form-control"
                                    id="password"
                                    name="password"
                                    autoComplete="password"
                                    ref={signup} />
                            </div>

                            <div class="form-row">
                                <div class="col">
                                    <label for="first_name">Address</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="first_name"
                                        name="first_name"
                                        autoComplete="given-name"
                                        ref={signup} />
                                </div>
                                <div class="col">
                                    <label for="last_name">Country</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="last_name"
                                        name="last_name"
                                        autoComplete="family-name"
                                        ref={signup} />
                                </div>
                            </div>



                        </form>
                    </div>

                </div>

                <div className="col-lg-5">

                </div>
            </div>
        </div>

    )
}

export default CustomerSignUp;