import React from "react";
import { useForm } from "react-hook-form";
import { AgentService } from "../../Services";

const AgentRegistrationForm = (props) => {
  const { history } = props;
  const onSubmit = async (data) => {
    AgentService.createNewAdmin(data, history);
  };
  const { register, handleSubmit } = useForm();

  return (
    <div className="container">
      <div class="py-5 text-center">
        <h2>Agent Registration</h2>
        <p class="lead">
          After reviewed by an administrator you will receive an email
        </p>
      </div>
      <div class="col-md-8 order-md-1">
        <form
          class="needs-validation"
          onSubmit={handleSubmit(onSubmit)}
          novalidate
        >
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="firstName">First name</label>
              <input
                ref={register}
                type="text"
                class="form-control"
                name="firstName"
                id="firstName"
                placeholder="First Name"
                required
              />
              <div class="invalid-feedback">Valid first name is required.</div>
            </div>
            <div class="col-md-6 mb-3">
              <label for="lastName">Last name</label>
              <input
                ref={register}
                type="text"
                class="form-control"
                name="lastName"
                id="lastName"
                placeholder="Last Name"
                required
              />
              <div class="invalid-feedback">Valid last name is required.</div>
            </div>
            <div class="col-md-6">
              <label for="email">
                Email <span class="text-muted">(Optional)</span>
              </label>
              <input
                ref={register}
                type="email"
                class="form-control"
                name="email"
                id="email"
                placeholder="you@example.com"
              />
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div class="col-md-6">
              <label for="contactNo">Contact No</label>
              <input
                ref={register}
                type="text"
                class="form-control"
                name="contactNo"
                id="contactNo"
                placeholder="Contact No"
                required
              />
              <div class="invalid-feedback">Please enter your contact no.</div>
            </div>

            <div class="col-md-6">
              <label for="address">Address</label>
              <input
                ref={register}
                type="text"
                class="form-control"
                name="addressNo"
                id="address"
                placeholder="1234 Main St"
                required
              />
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="col-md-6">
              <label for="address2">Address 2</label>
              <input
                ref={register}
                type="text"
                class="form-control"
                name="addressStreet"
                id="address2"
                placeholder="Apartment or suite"
              />
            </div>

            <div class="col-md-6">
              <label for="country">Country</label>
              <select
                ref={register}
                class="custom-select d-block w-100"
                name="addressCountry"
                id="country"
                required
              >
                <option value="">Choose...</option>
                <option>United States</option>
              </select>
              <div class="invalid-feedback">Please select a valid country.</div>
            </div>

            <div class="col-md-6">
              <label for="serviceArea">Service Area</label>
              <select
                ref={register}
                class="custom-select d-block w-100"
                name="serviceArea"
                id="serviceArea"
                required
              >
                <option value="">Choose...</option>
                <option>Samoa Islands</option>
              </select>
              <div class="invalid-feedback">
                Please select a valid service area.
              </div>
            </div>
            <div class="col-md-6">
              <label for="password">
                Password <span class="text-muted">(Optional)</span>
              </label>
              <input
                ref={register}
                type="password"
                class="form-control"
                name="password"
                id="password"
                placeholder="Password"
              />
              <div class="invalid-feedback">Please enter a valid password.</div>
            </div>

            <div class="col-md-6">
              <label for="cpassword">Confirm Password</label>
              <input
                ref={register}
                type="password"
                class="form-control"
                name="cpassword"
                id="cpassword"
                placeholder="Confirm Password"
                required
              />
              <div class="invalid-feedback">
                Please enter your password again.
              </div>
            </div>
          </div>
          <br />
          <hr />
          <button class="btn btn-primary btn-lg btn-block" type="submit">
            Continue To Register
          </button>
        </form>
      </div>
    </div>
  );
};

export default AgentRegistrationForm;
