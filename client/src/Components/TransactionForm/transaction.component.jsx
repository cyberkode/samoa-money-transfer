import React from "react";
import { useForm } from "react-hook-form";
import { withRouter } from 'react-router-dom';
import { TransactionService } from "../../Services";

import "./transaction.styles.scss";

const Transaction = (props) => {

  const { register, handleSubmit } = useForm();
  const onSubmit = async (data) => {
    const { history } = props;
    TransactionService.CreateTransaction(data, history);
  };

  return (
    <div className="container col-md-4">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <label className="recipientName">Recipient Name</label>
          <input
            type="text"
            ref={register}
            name="recipientName"
            className="form-control"
            id="recipientName"
            required
            placeholder="Recipient Name"
          />
        </div>
        <div className="form-group">
          <label className="re_country">Recipient Country</label>
          <select
            name="re_country"
            className="form-control"
            id="re_country"
            ref={register}
          >
            <option>New Zealand</option>
            <option>Samoa Islands</option>
          </select>
        </div>
        <div className="form-group">
          <label className="agent_list">Select Agent</label>
          <select
            name="agent_list"
            className="form-control"
            id="agent_list"
            ref={register}
          >
            <option>Pamuditha</option>
            <option>Samoa Islands</option>
          </select>
        </div>
        <div className="form-group">
          <label className="recipientName">Bank Name</label>
          <input
            type="text"
            ref={register}
            name="bankName"
            className="form-control"
            id="bankName"
            required
            placeholder="Bank Name"
          />
        </div>
        <div className="form-group">
          <label className="recipientName">BSB Number</label>
          <input
            type="text"
            ref={register}
            name="bsbNo"
            className="form-control"
            id="bsbNo"
            required
            placeholder="BSB Number"
          />
        </div>
        <div className="form-group">
          <label className="recipientName">Account Number</label>
          <input
            type="text"
            ref={register}
            name="acc_no"
            className="form-control"
            id="acc_no"
            required
            placeholder="Account Number"
          />
        </div>
        <div className="form-group">
          <label className="recipientName">Amount</label>
          <input
            type="text"
            ref={register}
            name="amount"
            className="form-control"
            id="amount"
            required
            placeholder="Amount"
          />
        </div>
        <button type="submit" className="btn btn-primary btn-lg btn-block">
          Primary
        </button>
      </form>
    </div>
  );
};

export default withRouter(Transaction);
