import React from "react";

import './navbar.css';

const Navbar = (props) => {
  return (
    <div style={{marginBottom : '20px'}}>
      <header className="header fixed-top">
        <div className="branding">
          <div className="container-fluid position-relative">
            <nav className="navbar navbar-expand-lg">
              <div className="site-logo">
                <a className="navbar-brand" href="/">
                 { /*<img
                    className="logo-icon mr-2"
                    src="assets/images/site-logo.svg"
                    alt="something"
                 /> */}
                  <span className="logo-text">
                    Samoa<span className="text-alt">Moneytransfer</span>
                  </span>
                </a>
              </div>

              <button
                className="navbar-toggler collapsed"
                type="button"
                data-toggle="collapse"
                data-target="#navigation"
                aria-controls="navigation"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span> </span>
                <span> </span>
                <span> </span>
              </button>

              <div
                className="collapse navbar-collapse py-3 py-lg-0"
                id="navigation"
              >
                <ul className="navbar-nav ml-lg-auto">
                  <li className="nav-item mr-lg-4">
                    <a className="nav-link" href="!#">
                    Home
                    </a>
                  </li>
                  <li className="nav-item mr-lg-4">
                    <a className="nav-link" href="!#">
                    About Us
                    </a>
                  </li>
                  <li className="nav-item mr-lg-4">
                    <a className="nav-link" href="!#">
                    Contact
                    </a>
                  </li>
                  <li className="nav-item mr-lg-4">
                    <a className="nav-link" href="!#">
                    Functions
                    </a>
                    </li>
                    <li className="nav-item mr-lg-4">
                    <a className="nav-link" href="!#">
                    Login
                    </a>
                  </li>
                  <li className="nav-item mr-lg-4">
                    <a className="nav-link" href="!#">
                    Registration
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </header>
    </div>
  );
};

export default Navbar;
