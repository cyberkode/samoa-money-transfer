import React, { Component } from 'react';
import './App.css';
import AgentRegistrationPage from './pages/AgentRegistrationPage/agentRegistration.page';
import testPage from './pages/Trans_Form/testingPage';
import {Route, BrowserRouter as Router,Switch} from 'react-router-dom';
import NavBar from './components/NavBar/navbar.component';
import LandingPage from './pages/LandingPage/LandingPage'
import CustomerRegistrationPage from './pages/CustomerRegistrationPage/customerRegistration'
import Footer from './components/Footer/Footer';
import AgentDashboardPage from './pages/AgentDashboardPage/AgentDashboardPage'
import CustomerLoginPage from './pages/CustomerLoginPage/customerLogin'


class App extends Component{
  
  render() {
    const App = () => (
      <div>
        <Switch>
          <Route exact path='/' component={LandingPage}/>
          <Route path='/agentRegistration' component={AgentRegistrationPage}/>
          <Route path='/test' component={testPage}/>
          <Route path='/agentDashboard' component={AgentDashboardPage}/>
          <Route path='/customerRegistration' component={CustomerRegistrationPage}/>
          <Route path='/customerLogin' component={CustomerLoginPage}/>
        </Switch>
      </div>
    )
    return (
      <Router>
        <NavBar/>
      <Switch>
        <App/>
      </Switch>
      <Footer/>
      </Router>
    );
  }
}

export default App;
