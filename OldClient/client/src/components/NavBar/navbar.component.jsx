import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";

import "./navbar.styles.css";

class NavBar extends Component {
  render() {
    return (
      <AppBar
        position="sticky"
        color="primary"
        elevation={0}
        className="appBar"
      >
        <Toolbar className="toolbar">
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className="toolbarTitle"
          >
            Samoa Money Transfer
          </Typography>
          <nav>
            <Link
              variant="button"
              color="textPrimary"
              href="/"
              className="link"
            >
              Home
            </Link>
            <Link
              variant="button"
              color="textPrimary"
              href="/agentRegistration"
              className="link"
            >
              Agent Registration
            </Link>
            <Link
              variant="button"
              color="textPrimary"
              href="/superAdmin"
              className="link"
            >
              Super Admin
            </Link>
            <Link
              variant="button"
              color="textPrimary"
              href="/customerRegistration"
              className="link"
            >
              Customer Registration
            </Link>
            <Link
              variant="button"
              color="textPrimary"
              herf="/agentDashboard"
              className="link"
            >
              Agent Dashboard
            </Link>
            <Link
              variant="button"
              color="textPrimary"
              href="/test"
              className="link"
            >
              Transaction
            </Link>
          </nav>
          <Button
            href="/customerLogin"
            color="white"
            variant="outlined"
            className="link"
          >
            Login
          </Button>
        </Toolbar>
      </AppBar>
    );
  }
}

export default NavBar;
