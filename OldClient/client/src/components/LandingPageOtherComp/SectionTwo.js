import React, { Component } from "react";
import "./sectionTwo.css";
import { Container } from "@material-ui/core";
import SPart1 from "./img/sPart1.svg";
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';

class SectionTwo extends Component {
  render() {
    return (
      <div className="section-two-main">
        <Container>
          <div className="section-two-parts">
            <div className="section-part-1">
              <img src={SPart1} alt="Spart1" />
            </div>
            <div className="section-part-2">
            <div className="benefits-main">
              <Benefits discription="Safe and reliable transfers"/>
              <Benefits discription="Great rates and low fees"/>
              <Benefits discription="Locations worldwide"/>
              </div>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

function Benefits ({discription}) {
  return(
      <div className = "benefits">
      <div className= "icon-div"><CheckBoxOutlinedIcon style={{fontSize: 60 , color:'#3498db'}}/></div>
      <div className="discription-div"><p>{discription}</p></div>
      </div>
  )
}

export default SectionTwo;
