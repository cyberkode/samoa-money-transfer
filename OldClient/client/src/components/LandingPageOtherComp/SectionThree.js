import React, { Component } from "react";
import "./sectionThree.css";
import { Container } from "@material-ui/core";

class SectionThree extends Component {
  render() {
    return (
      <div className='section-three-main'>
      <Container>
      <div className="context">
      <h1>We’re building money without borders.</h1>
      <p>The world’s banking systems weren’t designed for people without borders. That’s why we’re building a new one.</p>
      </div>
      </Container>
      <MoveingBackground/>
      </div>
    );
  }
}

class MoveingBackground extends Component{
  render(){
    return(
      <div class="area">
      <ul class="circles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>

      </ul>
  </div>
    )
  }
}

export default SectionThree;
