import React from "react";
import Axios from 'axios';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";


const TransactionModel = (propertice) => (
  <tr>
    <td>{propertice.transactionDetails.customer_code}</td>
    <td>{propertice.transactionDetails.agent_code}</td>
    <td>{propertice.transactionDetails.reciever_name}</td>
    <td>{propertice.transactionDetails.from}</td>
    <td>{propertice.transactionDetails.bank_name}</td>
    <td>
  </td>
  </tr>
);

class GetTransaction extends React.Component {
  constructor() {
    super();

    this.state = {transactionDetails : [] }

  }

  componentDidMount() {
      Axios.get('http://localhost:5000/transaction/').then(response => {
          this.setState ({ transactionDetails : response.data })
      })
      .catch((error) => {
          console.log(error);
      })
  }

  tranactionList() {
      return this.state.transactionDetails.map(allTransaction => {
          return <TransactionModel transactionDetails = {allTransaction} />
      })
  }

  render() {
      return (
          <div>
          <tbody>
          { this.tranactionList() }
        </tbody>
          </div>
      )
  }
}

export default GetTransaction;
