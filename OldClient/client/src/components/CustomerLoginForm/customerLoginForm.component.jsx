import React, { Component } from 'react';
import { Grid, Typography, TextField, Container, Divider, Button } from '@material-ui/core';
import axios from 'axios';
import './formStyles.css'
import swal from 'sweetalert';

class CustomerLoginForm extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    async onSubmit(e) {
        e.preventDefault()
        const userData = {
            email : this.state.email,
            password : this.state.password
        }

        let response = await axios.post('http://localhost:5000/customer/signin/', userData);
        console.log(response)

        // await axios.post('http://localhost:5000/customer/signin/', userData).then(res => {
        //     if(res.status === 200){
        //         console.log("Login Success")
        //         localStorage.setItem('usertoken', res.data)
        //         this.props.history.push(`/profile`)
        //     }else if(res.status === 404){
        //         swal({
        //             title: "Error",
        //             text: "Please check email and password",
        //             icon: "error",
        //             button: "Go Back",
        //         });
        //     }else{
        //         swal({
        //             title: "Error",
        //             text: "Please check email and password",
        //             icon: "error",
        //             button: "Go Back",
        //         });
        //     }
        // }).catch(error => {
        //     if(error){
        //         swal({
        //             title: "Error",
        //             text: "Something went wrong",
        //             icon: "error",
        //             button: "Go Back",
        //         });
        //     }
        // })
    }


    render() {
        return (
            <Container maxWidth="sm">
                <div id='paperForm'>
                    <Typography component="h1" variant="h5">Login to Samoa Money Transfer</Typography>
                    <Divider light style={{ marginBottom: '5vh' }} />
                    <form noValidate onSubmit={this.onSubmit}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    style={{ marginBottom: '5vh' }}
                                >
                                    Login
                            </Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>

        );
    }
}

export default CustomerLoginForm;