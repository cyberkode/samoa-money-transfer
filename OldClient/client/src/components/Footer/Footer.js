import React from "react";
import "./footer.css";
import { Container } from "@material-ui/core";

function Footer() {
  return (
    <div className="footer-section-main">
      <Container>
        <div className="footer-main">
          <div className="footer-main-part-one">
            <div className="footer-link-row">
              <div className="footer-link-header">
                <h1>TRANSFER</h1>
              </div>
              <div className="footer-links">
                <ul>
                  <li>About</li>
                  <li>Become an Agent</li>
                  <li>Compliance Guarantee</li>
                </ul>
              </div>
            </div>
            <div className="footer-link-row">
              <div className="footer-link-header">
                <h1>SUPPORT</h1>
              </div>
              <div className="footer-links">
                <ul>
                  <li>Contact</li>
                  <li>FAQs</li>
                  <li>Security</li>
                </ul>
              </div>
            </div>
            <div className="footer-link-row">
              <div className="footer-link-header">
                <h1>CONNECT</h1>
              </div>
              <div className="footer-links">
                <ul>
                  <li>Facebool</li>
                  <li>Twitter</li>
                  <li>Instergram</li>
                  <li>Blog</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="footer-main-part-two">
          <div className="footer-main-part-two-1">
            <div className="footer-part-two-items"><h4>Privacy Policy</h4></div>
            <div className="footer-part-two-items"></div>
            <div className="footer-part-two-items"><h4>CCPA Rights </h4></div>
            <div className="footer-part-two-items"></div>
            <div className="footer-part-two-items"><h4>Terms & Conditions</h4></div>
           </div>
           <div className="footer-main-part-two-2">
           <h4>Transfer Services @ 2020 developed by Cyberkode. All rights reserved.</h4>
           </div>
          </div>
        </div>
      </Container>
    </div>
  );
}

export default Footer;
