import React, { Component } from 'react';
import { Grid, Typography, TextField, Container, Divider, Button } from '@material-ui/core';
import axios from 'axios';
import './formStyles.css'
import swal from 'sweetalert';

class CustomerRegistrationForm extends Component {
    constructor() {
        super();
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            phone: '',
            country: '',
            address: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()
        console.log(this.state.first_name);
        const userData = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            password: this.state.password,
            phone: this.state.phone,
            country: this.state.country,
            address: this.state.address
        }
        
        axios.post('http://localhost:5000/customer/signup/', userData).then(res => {
            if (res.status === 200) {
                console.log("User created" +res.statusText)
                swal({
                    title: "Success",
                    text: "You've successfully signed up!",
                    icon: "success",
                    button: "Continue!",
                });
            } else if(res.status === 401) {
                swal({
                    title: "Error",
                    text: "Something went wrong",
                    icon: "error",
                    button: "Go Back",
                });
            } else {
                swal({
                    title: "Error",
                    text: "Something went wrong",
                    icon: "error",
                    button: "Go Back",
                }); 
            }
        }).catch(error => {
            swal({
                title: "Error",
                text: "Something went wrong",
                icon: "error",
                button: "Go Back",
            });
        })
    }

    render() {
        return (
            <Container maxWidth="sm">
                <div id='paperForm'>
                    <Typography component="h1" variant="h5">Create an account and continue using our services</Typography>
                    <Divider light style={{ marginBottom: '5vh' }} />
                    <form noValidate onSubmit={this.onSubmit}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <TextField
                                    onChange={this.onChange}
                                    autoComplete="fname"
                                    name="first_name"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="first_name"
                                    label="First Name"
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="last_name"
                                    label="Last Name"
                                    name="last_name"
                                    autoComplete="lname"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                            </Grid>
                            <Grid item lg={7} xs={6} sm={3}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="address"
                                    label="Address"
                                    id="address"
                                    autoComplete="address"
                                />
                            </Grid>
                            <Grid item lg={5} xs={6} sm={3}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="country"
                                    label="Country"
                                    id="country"
                                    autoComplete="country"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    onChange={this.onChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="phone"
                                    label="Phone Number"
                                    id="phone"
                                    autoComplete="phone"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    type="submit"
                                    size="large"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    style={{ marginBottom: '5vh' }}
                                >
                                    Register
                            </Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>

        );
    }
}

export default CustomerRegistrationForm;