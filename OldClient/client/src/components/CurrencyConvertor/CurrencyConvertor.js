import React, { Component } from "react";
import "./CConvertor.css";
import CountrySelect from "./CountrySelect";
import bg1 from "./img/5222.jpg";
import { Container, TextField } from "@material-ui/core";
import CurrencyYouGet from "./CurrencyYouGet";

class CurrencyConvertor extends Component {

  render() {

    return (

      <div className="c-main" >

        <CcBgImg1 />

        <Container>

          <div className="cc-body">

            <h1>
              Simple, secure international money <br /> transfer
            </h1>

            <div className="cc-convert">

              <div className="cc-fields">

                <div className="cc-input-fields">

                  <CountrySelect lable="Send to" />

                </div>
                <div className="cc-input-fields">

                  <TextField
                    id="outlined-number"
                    label="Amount"
                    type="number"
                    helperText="Sending Amount in USD"
                    variant="outlined"
                    fullWidth
                  />

                </div>

                <div className="cc-input-fields">

                  <CurrencyYouGet />

                </div>

              </div>

              <div className="coutinue-btn-div">

                <button className="coutinue-btn">Continue</button>

              </div>

            </div>

          </div>

        </Container>

      </div>

    );

  }

}

class CcBgImg1 extends Component {

  render() {

    return (

      <div className="ccBgImage">

        <div className="bg-1">

          <img src={bg1} alt="bg1" />

        </div>

      </div>

    );

  }

}

export default CurrencyConvertor;
