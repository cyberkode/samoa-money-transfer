import React from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";

const countries = [
  {
    value: "New Zeland",
    label: "New Zeland",
  },
  {
    value: "Sri Lanka",
    label: "Sri Lanka",
  },
  {
    value: "Australia",
    label: "Australia",
  },
  {
    value: "Canada",
    label: "Canada",
  },
];
const useStyles = makeStyles(theme => ({
  root: {
    "& .MuiTextField-root": {
      width: "26ch",
      borderColor: "#3498db"
    }
  }
}));

function CountrySelect(props) {
  const classes = useStyles();
  const [country, setCountry] = React.useState("EUR");

  const handleChange = (event) => {
    setCountry(event.target.value);
  };
  return (
    <div className={classes.root} >
      <TextField
        id="outlined-select-currency"
        select
        label={props.lable}
        value={country}
        onChange={handleChange}
        helperText="Please select Sending country"
        variant="outlined"
        fullWidth
      >
        {countries.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
    </div>
  );
}
export default CountrySelect;
