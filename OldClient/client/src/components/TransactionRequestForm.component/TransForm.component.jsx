import React from "react";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";

import axios from "axios";

class Transaction_Form extends React.Component {
  constructor() {
    super();

    this.state = {
      recipientName: "",
      re_country: "",
      bankName: "",
      bsbNo: "",
      accNo: "",
      trAmount: "",
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    console.log(this.state);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    console.log("name :: " + e.target.name);
    console.log("value :: " + e.target.value);
  }

  onSubmit(e) {
    e.preventDefault();
    const tranactionData = {
      recipientName: this.state.recipientName,
      re_country: this.state.re_country,
      bankName: this.state.bankName,
      bsbNo: this.state.bsbNo,
      accNo: this.state.accNo,
      trAmount: this.state.trAmount,
    };

    console.log(tranactionData);

    axios
      .post("http://localhost:5000/transaction/reqtransaction", tranactionData)
      .then((res) => {
        if (res.status === 200) {
          console.log("Transaction created");
        } else if (res.status === 400) {
          console.log(res.statusText);
        } else {
          console.log("Something went wrong");
        }
      });
  }

  componentDidMount() {
    
  }

  render() {
    return (
      <Container component="main" maxWidth="xs">
        <div>
          <form onSubmit={this.onSubmit} id="transactinForm" noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  onChange={this.onChange}
                  autoComplete="r_name"
                  name="recipientName"
                  variant="outlined"
                  required
                  fullWidth
                  id="recipientName"
                  label="Recipient Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <FormControl variant="outlined" fullWidth>
                  <InputLabel htmlFor="r_country_label">
                    Recipient Country
                  </InputLabel>
                  <Select
                    name="re_country"
                    labelId="r_country_label"
                    id="re_country"
                    value={this.state.re_country}
                    onChange={this.onChange}
                    label="Recipient Country"
                    required
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    <MenuItem value="New_Zealand">New Zealand</MenuItem>
                    <MenuItem value="Samoa_Islands">Samoa Islands</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <FormControl variant="outlined" fullWidth>
                  <InputLabel htmlFor="agent_list">
                    Select Agent
                  </InputLabel>
                  <Select
                    name="agent_li"
                    labelId="agent_lil"
                    id="agent_li"
                    value={this.state.re_country}
                    onChange={this.onChange}
                    label="Select Agent"
                    required
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    <MenuItem value="New_Zealand">New Zealand</MenuItem>
                    <MenuItem value="Samoa_Islands">Samoa Islands</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  onChange={this.onChange}
                  autoComplete="bank_name"
                  name="bankName"
                  variant="outlined"
                  required
                  fullWidth
                  id="bankName"
                  label="Bank Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  onChange={this.onChange}
                  autoComplete="bsb_no"
                  name="bsbNo"
                  variant="outlined"
                  required
                  fullWidth
                  id="bsbNo"
                  label="BSB Number"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  onChange={this.onChange}
                  autoComplete="acc_no"
                  name="accNo"
                  variant="outlined"
                  required
                  fullWidth
                  id="accNo"
                  label="Account Number"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  onChange={this.onChange}
                  autoComplete="amount"
                  name="trAmount"
                  variant="outlined"
                  required
                  fullWidth
                  id="trAmount"
                  label="Amount"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  variant="outlined"
                  color="primary"
                  fullWidth
                >
                  Primary
                </Button>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    );
  }
}

export default Transaction_Form;
