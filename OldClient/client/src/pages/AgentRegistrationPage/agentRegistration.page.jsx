import React,{Component} from 'react';
import Grid from '@material-ui/core/Grid';
import AgentRegistrationForm from '../../components/AgentRegistrationForm/agentRegistrationForm.component';


class AgentRegistrationPage extends Component{

render(){
    return(
        <Grid container spacing={3}>
        <Grid item xs={12}>
          <AgentRegistrationForm/>
          </Grid>
        </Grid>
           
    );
}
}

export default AgentRegistrationPage;