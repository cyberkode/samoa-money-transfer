import React, { Component } from 'react';
import { Container, Grid, Box } from '@material-ui/core';
import CustomerLoginForm from '../../components/CustomerLoginForm/customerLoginForm.component'

class CustomerLogin extends Component {
    render() {
        return (
            <Container>
                <Box style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}>
                    <Grid container  >
                        <Grid item xs={0} md={3} style={{ marginTop: '20vh', paddingLeft: '5vh' }}>
                            {/* <Typography component="h1" variant="h5">Some graphical content to be added</Typography> */}
                        </Grid>

                        <Grid item xs={12} md={6}>
                            <CustomerLoginForm />    
                        </Grid>

                        <Grid item xs={6} md={3} style={{ marginTop: '20vh', paddingLeft: '5vh' }}>
                            {/* <Typography component="h1" variant="h5">Some graphical content to be added</Typography> */}
                        </Grid>
                    </Grid>
                </Box>


            </Container>
        );
    }
}

export default CustomerLogin;