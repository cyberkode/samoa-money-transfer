import React from "react";

import Grid from "@material-ui/core/Grid";

import "./test.scss";

import TransF from "../../components/TransactionRequestForm.component/TransForm.component";
import GetTr from "../../components/TransactionDetails.Component/TransctionDetails.component";

class Testing extends React.Component {
  render() {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <br></br>
            <TransF />
            <br></br>
            <GetTr />
            <br></br>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Testing;
