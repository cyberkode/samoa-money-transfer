import React, { Component } from 'react';
import { Container, Grid, Typography, Box } from '@material-ui/core';
import CustomerRegistrationForm from '../../components/CustomerRegistrationFrom/customerRegistrationForm.component'

class CustomerRegistration extends Component {
    render() {
        return (
            <Container>
                <Box  style={{ justifyContent: 'center', alignItems: 'center', display: 'flex'  }}>
                    <Grid container  >
                        <Grid item xs={12} md={6}>
                            <CustomerRegistrationForm />
                        </Grid>
                        <Grid item xs={6} md={6} style={{ marginTop: '20vh', paddingLeft:'5vh'}}>
                            <Typography component="h1" variant="h5">Some graphical content to be added</Typography>
                        </Grid>
                    </Grid>
                </Box>


            </Container>
        );
    }
}

export default CustomerRegistration;