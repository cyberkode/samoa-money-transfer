import React, { Component } from 'react'
import CurrencyConvertor from '../../components/CurrencyConvertor/CurrencyConvertor'
import SectionTwo from '../../components/LandingPageOtherComp/SectionTwo'
import SectionThree from '../../components/LandingPageOtherComp/SectionThree'

class LandingPage extends Component{
    render(){
        return(
        <div>
        <CurrencyConvertor/>
        <SectionTwo/>
        <SectionThree/>
        </div>
        )
    }
}


export default LandingPage;