const jwt = require("jsonwebtoken");
process.env.SECRET_KEY = "secret";

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, process.env.SECRET_KEY);
    req.userData = decoded;
    next();
  } catch (error) {
    res.send({ code: 401, error: "Auth Failed" });
  }
};
