const express = require('express')
const agentRoutes = express.Router()
const AgentModel = require('../models/Agent')
const cors = require("cors")


agentRoutes.use(cors())

agentRoutes.route('/signup').post((req,res)=>{
    console.log("API called")
    const userData = {
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        agent_code : AgentCodeGenerator(),
        service_area : req.body.service_area,
        address : "Bar eka",
        email : req.body.email,
        contact_no : req.body.phone,
        password : req.body.password
    }
    // console.log(userData);
    AgentModel.create(userData).then(user => {
        res.status(200).json({status:'Success'})
    }).catch(error => {
        res.status(400).json({error: error})
    })
})

function AgentCodeGenerator(){
    let code = "SA"+(Math.floor(Math.random() * 90000) + 10000);
    return code;
}

module.exports = agentRoutes