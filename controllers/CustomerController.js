const express = require("express");
const customerRoutes = express.Router();
const CustomerModel = require("../models/Customer");
const cors = require("cors");
const webtoken = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const checkAuth = require("../services/auth_services/access_controller");

customerRoutes.use(cors());

process.env.SECRET_KEY = "secret";

customerRoutes.route("/signup").post((req, res) => {
  const userData = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    customer_code: CustomerCodeGenerator(),
    address: req.body.address,
    country: req.body.country,
    email: req.body.email,
    contact_no: req.body.phone,
    password: req.body.password,
  };

  CustomerModel.findOne({ email: userData.email })
    .then((user) => {
      if (!user) {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          userData.password = hash;
          CustomerModel.create(userData)
            .then((user) => {
              res.send({ code: 200, status: "Success" });
            })
            .catch((error) => {
              res.send({ code: 400, error: error });
            });
        });
      } else {
        res.send({ code: 400, error: "User already exists in the system" });
      }
    })
    .catch((error) => {
      res.send({ code: 400, error: error });
    });
});

customerRoutes.route("/signin").post((req, res) => {
  CustomerModel.findOne({
    email: req.body.email,
  })
    .then((user) => {
      if (user) {
        if (bcrypt.compareSync(req.body.password, user.password)) {
          const payload = {
            _id: user.customer_code,
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
          };
          let token = webtoken.sign(payload, process.env.SECRET_KEY, {
            expiresIn: "1h",
          });
          res.send({ code: 200, token: token });
        } else {
          res.send({
            code: 401,
            error: "Please check your email and password",
          });
        }
      } else {
        res.send({ code: 400, error: "Please check your email and password" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.send("Error: " + err);
    });
});

customerRoutes.get("/profile", checkAuth, (req, res) => {
  CustomerModel.findOne({
    email: req.userData.email,
  })
    .then((user) => {
      if (user) {
        res.send({ code: 200, user: user });
      } else {
        res.send({ code: 401, error: "Auth failed" });
      }
    })
    .catch((err) => {
      res.send({ error: "User does not exists" });
    });
});

function CustomerCodeGenerator() {
  let code = "SC" + (Math.floor(Math.random() * 90000) + 10000);
  return code;
}

module.exports = customerRoutes;
