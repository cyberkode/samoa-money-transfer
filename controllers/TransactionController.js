const express = require("express");
const transactionRoutes = express.Router();
const TransactionModel = require("../models/Transaction");
const cors = require("cors");

transactionRoutes.use(cors());

transactionRoutes.route("/reqtransaction").post((req, res) => {
  const tranactionData = {
    agent_code: req.body.agentName,
    reciever_name: req.body.recipinpentName,
    to: req.body.re_country,
    bank_name: req.body.bankName,
    bsb_num: req.body.bsbNo,
    reciever_account: req.body.accNo,
    amount: req.body.trAmount,
    date: "2010",
    reference_no: TransactionReferenceGenerator(),
    transfer_status: "In progress",
  };

  TransactionModel.create(tranactionData)
    .then((trans) => {
      res.status(200).json({ status: "success" });
    })
    .catch((error) => {
      res.status(400).json({ error: error });
      console.log(error);
    });
});

transactionRoutes.route("/").get((req, res) => {
  TransactionModel.find()
    .then((transactionDetails) => res.json(transactionDetails))
    .catch((err) => res.status(400).json("Error  : " + err));
  // console.log(transactionDetails);
});

transactionRoutes.route("/update/:id").post((req, res) => {
  TransactionModel.findById(req.params.id).then((transaction) => {
    (transaction.customer_code = req.body.customer_code),
      (transaction.agent_code = req.body.agent_code),
      (transaction.reciever_name = req.body.recipientName),
      (transaction.from = req.body.re_country),
      (transaction.bank_name = req.body.bankName),
      (transaction.bsb_num = req.body.bsbNo),
      (transaction.reciever_account = req.body.accNo),
      (transaction.amount = req.body.trAmount),
      (transaction.date = req.body.date),
      (transaction.reference_no = TransactionReferenceGenerator()),
      (transaction.transfer_status = req.body.transfer_status);
  });
});

function TransactionReferenceGenerator(){
  let reference = "ST" + Date.now();
  return reference;
}

module.exports = transactionRoutes;
